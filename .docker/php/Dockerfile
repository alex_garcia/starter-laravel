ARG VERSION

FROM php:${VERSION}

# Arguments defined in docker-compose.yml
ARG USER
ARG UID
ARG DB_TYPE
ARG TIME_ZONE
ARG PHPREDIS_VERSION
RUN ln -snf /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime && echo ${TIME_ZONE} > /etc/timezone


# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions

# Install PDO
RUN if [ ${DB_TYPE} = "mysql" ] \
    ; then docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd \
    ; else apt-get update && apt-get install -y libpq-dev && docker-php-ext-install pdo pdo_pgsql \ 
; fi

RUN mkdir -p /usr/src/php/ext/redis \
    && curl -L https://github.com/phpredis/phpredis/archive/$PHPREDIS_VERSION.tar.gz | tar xvz -C /usr/src/php/ext/redis --strip 1 \
    && echo 'redis' >> /usr/src/php-available-exts \
    && docker-php-ext-install redis

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u ${UID} -d /home/${USER} ${USER}
RUN mkdir -p /home/${USER}/.composer && \
    chown -R ${USER}:${USER} /home/${USER}

# Set working directory
WORKDIR /var/www

USER root